import React from 'react';
import Stack from '@mui/material/Stack';
import {useAppDispatch, useAppSelector} from '../../hooks/hooks';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, {AlertProps} from '@mui/material/Alert';
import { setError } from '../../App/App.reducer';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}/>
})

export default function CustomizedSnackbars() {

    const dispatch = useAppDispatch();

    const error = useAppSelector(state => state.App.error);

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        dispatch(setError({error: null}))
    }

    return (
        <Stack spacing={2} sx={{width: '100%'}}>
            <Snackbar open={error !== null} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{width: '100%'}}>{error}</Alert>
            </Snackbar>
        </Stack>
    )
}